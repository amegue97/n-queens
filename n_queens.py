from pprint import pprint



def create_board(n):
    board = []
    for i in range(n):
        row = []
        for i in range(n):
            row.append(0)
        board.append(row)    
    return board

# pprint(create_board(4), width=15)    


def check_sides(board, row, column):
    for idx in range(len(board)):
        if idx == column:
            continue
        if board[row][idx] == 1:
            return False 
    return True


def check_diagonal_below(board, row, column):
    r = row + 1
    c_left = column - 1
    c_right = column + 1       
    
    while r < len(board):
        while c_left >= 0:
            if board[r][c_left] == 1:
                return False
            else:
                c_left -= 1
                break 
        while c_right < len(board):
            if board[r][c_right] == 1:
                return False
            else:
                c_right += 1
                break 
        r += 1              
    return True


def check_diagonal_above(board, row, column):
    r = row - 1
    c_left = column - 1
    c_right = column + 1

    while r >= 0:
        while c_left >= 0:
            if board[r][c_left] == 1:
                return False
            else:
                c_left -= 1
                break 
        while c_right < len(board):
            if board[r][c_right] == 1:
                return False
            else:
                c_right += 1
                break     
        r -= 1          
    return True



def place_queen(board, column=0):     
    # We have placed a queen in the last column safely => Solution found!
    if column == len(board):
        return True

    for row in range(len(board)):
        if column == 0 and row == 0 and (len(board) == 4 or len(board) == 5):
            continue
        # Place a queen in the current row and current column 
        board[row][column] = 1
        # Check if that position is safe
        if check_sides(board, row, column) and check_diagonal_above(board, row, column) and check_diagonal_below(board, row, column):
            # If so, make a variable to call the function again
            next_move = place_queen(board, column + 1)
            # pprint(board, width = 15)
            # Return the result of place_queens (fucntion gets called again)
            return next_move
        # Not a safe move    
        else:
            # Therefore, remove the queen and move on to the next row 
            board[row][column] = 0 

    # board = create_board(len(board))
    # print("new board:", board)        
    # return board
    return False 


new_board = create_board(5)
if place_queen(new_board):
    pprint(new_board, width=24)
    print("SUCCESS!")
else:
    pprint(new_board, width=24)
    print("GAME OVER")          

